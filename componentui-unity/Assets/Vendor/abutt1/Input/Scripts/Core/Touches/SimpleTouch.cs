using UnityEngine;
using System.Collections;

namespace abutt1.ProcessedInput
{
	public struct SimpleTouch {
		
		public  int         touchId;
		public  Vector2     touchPos;
		public  Vector2     touchDeltaPos;
		public  float       touchDeltaTime;
		public  int         touchTapCount;
		public  TouchPhase  touchPhase;
		
		public SimpleTouch(Touch fromUnityTouch)
		{
			touchId        = fromUnityTouch.fingerId;
			touchPos       = fromUnityTouch.position;
			touchDeltaPos  = fromUnityTouch.deltaPosition;
			touchDeltaTime = fromUnityTouch.deltaTime;
			touchTapCount  = fromUnityTouch.tapCount;
			touchPhase     = fromUnityTouch.phase;
		}
		
	}
}
