using UnityEngine;
using System.Collections;

namespace abutt1.ProcessedInput
{
	public enum InputProcessingBehaviourType
	{
		None,				// none
		InvertedX,			// inverted X axis
		InvertedY,			// inverted Y axis
		InvertedXY,			// inverted X axis, inverted Y axis,
	}	
		
	public enum TouchState
	{
		Inactive,           // inactive
		Down,               // first down
		Active,             // still down
		Up,                 // up
	}
	
	
	public interface ITouch
	{
		int                           TouchID                 { get; }
		bool                          IsActive                { get; }
		
		TouchState                    CurrentState            { get; }
		
		Vector2                       CurrentPos              {	get; }
		Vector2                       CurrentPosRaw           {	get; }
		Vector2                       InitialPos              {	get; }
		Vector2                       InitialPosRaw           {	get; }
		Vector2                       CurrentDelta            {	get; }
		Vector2                       CurrentDeltaRaw         {	get; }
		Vector2                       TotalDelta              {	get; }
		Vector2                       TotalDeltaRaw           {	get; }
		float                         DeltaDistance           {	get; }
		float                         TotalDistance           {	get; }
		float                         TapDuration             {	get; }
		InputProcessingBehaviourType  ProcessBehaviour        {	get; }
	}
	
	public struct CachedTouch : ITouch
	{		
		private int                             id;
		private InputProcessingBehaviourType    processBehaviour;
		private TouchState                      currentState;
		private float                           startTime;
		private float                           tapDuration;
		private Vector2                         initialPos;
		private Vector2                         currentPos;
		private Vector2                         currentDelta;
		
		public int                           TouchID                 {  get {   return id;                                                  }	}
		public bool                          IsActive                {  get {   return CalculateCurrentlyActive();                          }	}
		
		public TouchState                    CurrentState            {  get {   return currentState;                                        }	}
		
		public Vector2                       CurrentPos              {	get	{	return ProcessPosition(processBehaviour, CurrentPosRaw);    }	}
		public Vector2                       CurrentPosRaw           {	get	{	return currentPos;                                          }	}
		public Vector2                       InitialPos              {	get	{	return ProcessPosition(processBehaviour, initialPos);       }	}
		public Vector2                       InitialPosRaw           {	get	{	return initialPos;                                          }	}
		public Vector2                       CurrentDelta            {	get	{	return ProcessDelta(processBehaviour, CurrentDeltaRaw);     }	}
		public Vector2                       CurrentDeltaRaw         {	get	{	return currentDelta;                                        }	}
		public Vector2                       TotalDelta              {	get	{	return ProcessDelta(processBehaviour, TotalDeltaRaw);       }	}
		public Vector2                       TotalDeltaRaw           {	get	{	return currentPos-initialPos;                               }	}
		public float                         DeltaDistance           {	get	{	return Vector2.Distance(Vector2.zero, currentDelta);        }	}
		public float                         TotalDistance           {	get	{	return Vector2.Distance(currentPos, initialPos);            }	}
		public float                         TapDuration             {	get	{	return tapDuration;                                         }	}
		public InputProcessingBehaviourType  ProcessBehaviour        {	get	{	return processBehaviour;                                    }	}
		
		public void SetPosition(Vector2 toPos, bool touchFinished = false)
		{
			if(!IsActive)
			{
				currentState = TouchState.Down;
				
				startTime    = Time.realtimeSinceStartup;
				
				initialPos   = toPos;
				currentPos   = toPos;
				currentDelta = new Vector2(0,0);
			}
			else
			{
				currentState = TouchState.Active;
				
				currentDelta = toPos - currentPos;
				currentPos   = toPos;
			}
			
			tapDuration = Time.realtimeSinceStartup - startTime;
			
			if(touchFinished) {
				FinishTouch();
			}
		}
		
		public void FinishTouch()
		{
			if(IsActive) {
				currentState = TouchState.Up;
				
				// send out an event if necessary
				
			} else {
				currentState = TouchState.Inactive;
			}
		}
		
		public void SetProcessingBehaviour(InputProcessingBehaviourType toType)
		{
			processBehaviour = toType;
		}
		
		public void SetID(int toID)
		{
			id = toID;
		}
		
		static Vector2 ProcessPosition(InputProcessingBehaviourType processingBehaviour, Vector2 toProcess)
		{
			switch(processingBehaviour)
			{
				case(InputProcessingBehaviourType.InvertedX):
				{
					return new Vector2(Screen.width - toProcess.x, toProcess.y);
				}
				case(InputProcessingBehaviourType.InvertedY):
				{
					return new Vector2(toProcess.x, Screen.height - toProcess.y);
				}
				case(InputProcessingBehaviourType.InvertedXY):
				{
					return new Vector2(Screen.width - toProcess.x, Screen.height -toProcess.y);
				}
				case(InputProcessingBehaviourType.None):
				default:
				{
					return toProcess;
				}
			}		
		}
	
		static Vector2 ProcessDelta(InputProcessingBehaviourType processingBehaviour, Vector2 toProcess)
		{
			switch(processingBehaviour)
			{
				case(InputProcessingBehaviourType.InvertedX):
				{
					return new Vector2(-toProcess.x, toProcess.y);
				}
				case(InputProcessingBehaviourType.InvertedY):
				{
					return new Vector2(toProcess.x, -toProcess.y);
				}
				case(InputProcessingBehaviourType.InvertedXY):
				{
					return new Vector2(-toProcess.x, -toProcess.y);
				}
				case(InputProcessingBehaviourType.None):
				default:
				{
					return toProcess;
				}
			}
		}
		
		private bool CalculateCurrentlyActive()
		{
			return (currentState == TouchState.Down || currentState == TouchState.Active);
		}
	}
}
