using UnityEngine;
using System.Collections;

namespace abutt1.ProcessedInput
{
	public interface IInputModifier {
	
		Vector2 ModifyTouch(Vector2 toParse);
		
	}
	
	public abstract class MonoInputModifier : MonoBehaviour, IInputModifier
	{
		abstract public Vector2 ModifyTouch(Vector2 toParse);
	}
}