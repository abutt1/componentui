using UnityEngine;
using System.Collections;

namespace abutt1.ProcessedInput
{
	public interface IInputParser {
	
		bool TouchIsTap(CachedTouch touch);
		
	}
	
	public abstract class MonoInputParser : MonoBehaviour, IInputParser
	{
		abstract public bool TouchIsTap(CachedTouch touch);
	}
}
