using UnityEngine;
using System.Collections;

namespace abutt1.ProcessedInput
{
	public interface IInputProvider {
	
		SimpleTouch[] GetTouches();
		
	}
	
	public abstract class MonoInputProvider : MonoBehaviour, IInputProvider
	{
		abstract public SimpleTouch[] GetTouches ();
	}
}
