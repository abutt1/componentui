using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace abutt1.ProcessedInput
{
	public delegate void  InputControllerDelegate(ITouch relevantTouch);
	public delegate void  InputControllerUpdatedDelegate(IEnumerable<ITouch> relevantTouches);
	
	public interface IInputControllerStatus
	{
		event   InputControllerUpdatedDelegate  TouchesCalculatedEvent;
		
		event   InputControllerDelegate         TouchStartedEvent;
		event   InputControllerDelegate         TouchFinishedEvent;
		event   InputControllerDelegate         TapFinishedEvent;
		
		IEnumerable<ITouch>                     ActiveTouches               { get;      }
	
		int                                     MaxTouchCount               { get;      }
		InputProcessingBehaviourType            ProcessingBehaviour         { get;      }
	}
	
	public interface IInputController : IInputControllerStatus
	{
		void                                    CalculateTouches();
		
		new     int                             MaxTouchCount               { get; set; }
		new     InputProcessingBehaviourType    ProcessingBehaviour         { get; set; }
		
		void    SetInputProvider(IInputProvider toObj);
		void    SetInputModifier(IInputModifier toObj);
		void    SetInputParser  (IInputParser   toObj);
	}
	
	public class InputController {
		
		private static IInputController cachedInputImpl;
		public static IInputController SharedInstance
		{
			get
			{
				if(cachedInputImpl == null) { cachedInputImpl = new InputControllerImpl(); }
				return cachedInputImpl;
			}
			set
			{
				cachedInputImpl = value;
			}
		}
		
		public static IInputControllerStatus SharedStatus { get { return SharedInstance; } }
		
	}
	
	public class InputControllerImpl : IInputController
	{
		#region Fields
		
		#region Cached fields
		private IInputProvider          inputProvider                       = new StandardInputProvider();
		private IInputModifier          inputModifier;
		private IInputParser            inputParser;
		
		private int                     lastCapturedFrame                   = -1;
		private int                     maxTouchCount                       = 5;
		InputProcessingBehaviourType    defaultTouchProcessingBehaviour     = InputProcessingBehaviourType.InvertedY;
		
		private List<ITouch>            exportTouches                       = new List<ITouch>();
		private List<CachedTouch>       cachedTouches                       = new List<CachedTouch>();		
		#endregion

		#region Interface
		public event InputControllerUpdatedDelegate     TouchesCalculatedEvent;
		
		public event InputControllerDelegate            TouchStartedEvent;
		public event InputControllerDelegate            TouchFinishedEvent;
		public event InputControllerDelegate            TapFinishedEvent;			

		public IEnumerable<ITouch> ActiveTouches
		{
			get
			{
				CalculateTouches();
				return exportTouches;
			}
		}
		
		public int MaxTouchCount 
		{
			get { return maxTouchCount;                 }
			set { maxTouchCount = Mathf.Max(value, 0);  }
		}
		
		public InputProcessingBehaviourType ProcessingBehaviour
		{
			get { return defaultTouchProcessingBehaviour;   }
			set { defaultTouchProcessingBehaviour = value;  }
		}
		#endregion
		
		#endregion
		
		#region Functions
		
		#region Interface
		public void SetInputProvider (IInputProvider toObj) { inputProvider = toObj; }
		public void SetInputParser   (IInputParser toObj)   { inputParser   = toObj; }
		public void SetInputModifier (IInputModifier toObj) { inputModifier = toObj; }
		
		public void CalculateTouches ()
		{
			if(lastCapturedFrame >= Time.frameCount) { return; }
			
			#region Get Input
			
			SimpleTouch[] touches = inputProvider != null ? inputProvider.GetTouches() : new SimpleTouch[0];
						
			List<SimpleTouch> unusedTouches = new List<SimpleTouch>(touches); // touches that haven't been cached
			List<SimpleTouch> usedTouches   = new List<SimpleTouch>();        // touches that have been cached
			
			// grab out all used touches
			for (int i = 0; i < touches.Length; i++) {
				SimpleTouch t = touches[i];
				for (int j = 0; j < cachedTouches.Count; j++) {
					if(cachedTouches[j].TouchID == t.touchId)
					{
						// it's being used so remove it
						unusedTouches.Remove(t);
						usedTouches.Add(t);
					}
				}				
			}
			
			// if we still have some unused touches, and space to create new cached ones, create them
			while(unusedTouches.Count > 0 && cachedTouches.Count < MaxTouchCount)
			{
				SimpleTouch uT      = unusedTouches[0];    // unused touch
				
				CachedTouch newCT   = CreateCachedTouch(uT.touchId, defaultTouchProcessingBehaviour);
				
				cachedTouches.Add(newCT);
				
				unusedTouches.RemoveAt(0);
				usedTouches.Add(uT);
				
			}
			
			#endregion
			
			#region Apply Input
			
			// used to determine which touches are no longer being used
			bool[] currentlyActiveTouchIndices = new bool[cachedTouches.Count];
			
			#region Cache the Input Touches
			for(int i = 0; i < usedTouches.Count; i++)
			{
				SimpleTouch iT      = usedTouches[i];   // in touch
				int         cTIndex = -1;               // cached touch index
				
				
				for (int j = 0; j < cachedTouches.Count; j++) {
					if(iT.touchId == cachedTouches[j].TouchID)
					{
						cTIndex = j;
						break;
					}
				}
				
				if(cTIndex > -1)
				{
					CachedTouch cT = cachedTouches[cTIndex];
					
					bool touchFinished = (iT.touchPhase == TouchPhase.Ended || iT.touchPhase == TouchPhase.Canceled);
					
					currentlyActiveTouchIndices[cTIndex] = !touchFinished;
					
					bool shouldraiseTouchStartedEvent   = false;
					
					if(!touchFinished) {
						// if previously inactive, the touch has just begun
						shouldraiseTouchStartedEvent   = !cT.IsActive;
					}
					
					Vector2 parsedTouchPos = iT.touchPos;
					if(inputModifier != null)
					{
						parsedTouchPos = inputModifier.ModifyTouch(parsedTouchPos);
					}
					
					cT.SetPosition(parsedTouchPos);
					
					if(shouldraiseTouchStartedEvent)    { RaiseTouchStartedEvent(cT);   }
					
					cachedTouches[cTIndex] = cT;
				}
			}
			#endregion
			
			// finish any unfinished touches
			#region remove unfinished touches
			for(int i = currentlyActiveTouchIndices.Length -1; i >= 0; i--) {
				if(currentlyActiveTouchIndices[i] == false) {
					CachedTouch cT = cachedTouches[i];
					
					if(cT.IsActive)
					{
						// if still active, the touch has just finished.
						bool shouldRaiseTapFinishedEvent    = (inputParser == null || inputParser.TouchIsTap(cT) );
						bool shouldRaiseTouchFinishedEvent  = true;
	
						cT.FinishTouch();
						
						if(shouldRaiseTapFinishedEvent)     { RaiseTapFinishedEvent(cT);    }
						if(shouldRaiseTouchFinishedEvent)   { RaiseTouchFinishedEvent(cT);  }
						
						cachedTouches[i] = cT;
					}
					else
					{
						cachedTouches.RemoveAt(i);
					}
				}
			}
			#endregion
			
			#endregion
			
			
			exportTouches.Clear();
			for (int i = 0; i < cachedTouches.Count; i++) { exportTouches.Add(cachedTouches[i]); }
			
			lastCapturedFrame = Time.frameCount;
			
			RaiseTouchesCalculatedEvent(exportTouches);
		}		
		#endregion
				
		static CachedTouch CreateCachedTouch(int withID, InputProcessingBehaviourType withProcessType)
		{
			CachedTouch newCT = new CachedTouch();
			newCT.SetProcessingBehaviour(withProcessType);
			newCT.SetID(withID);
			return newCT;
		}

		#region Events
		void RaiseTouchStartedEvent(ITouch relevantTouch)
		{
			//Debug.Log("RaiseTouchStartedEvent");
			RaiseInputControllerDelegate(TouchStartedEvent, relevantTouch);
		}
	
		void RaiseTouchFinishedEvent(ITouch relevantTouch)
		{
			//Debug.Log("RaiseTouchFinishedEvent");
			RaiseInputControllerDelegate(TouchFinishedEvent, relevantTouch);
		}
		
		void RaiseTapFinishedEvent(ITouch relevantTouch)
		{
			//Debug.Log("RaiseTapFinishedEvent");
			RaiseInputControllerDelegate(TapFinishedEvent, relevantTouch);
		}
		
		void RaiseTouchesCalculatedEvent(IEnumerable<ITouch> relevantTouches)
		{
			if(TouchesCalculatedEvent != null) { TouchesCalculatedEvent(relevantTouches); }
		}
		
		static void RaiseInputControllerDelegate(InputControllerDelegate toRaise, ITouch relevantTouch)
		{
			if(toRaise != null) { toRaise(relevantTouch); }
		}
		#endregion
		
		#endregion
		
		
	}
	
}
