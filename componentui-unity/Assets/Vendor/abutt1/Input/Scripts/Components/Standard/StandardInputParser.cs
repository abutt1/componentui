using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace abutt1.ProcessedInput
{
	public class StandardInputParser : IInputParser {
		
		public StandardInputParser(float MaxDuration)
		{
			this.MaxDuration = MaxDuration;
		}
		
		public  float  MaxDuration;
		
		public bool TouchIsTap (CachedTouch touch)
		{
			bool ret = true;
			
			if(MaxDuration > 0 && MaxDuration < touch.TapDuration) { ret = false; } // duration was defined, but the tap was held for too long
			
			return ret;
		}
		
	}
}
