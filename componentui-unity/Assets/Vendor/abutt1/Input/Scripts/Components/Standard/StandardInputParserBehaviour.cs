using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace abutt1.ProcessedInput
{
	public class StandardInputParserBehaviour : MonoInputParser {
		
		public  float  MaxDuration;
		
		private StandardInputParser parserImpl;
		
		void Awake()
		{
			parserImpl = new StandardInputParser(MaxDuration);
		}
		
		void Update()
		{
			if(MaxDuration != parserImpl.MaxDuration)
			{
				parserImpl.MaxDuration = MaxDuration;
			}
		}
		
		public override bool TouchIsTap (CachedTouch touch)
		{
			return parserImpl.TouchIsTap(touch);
		}
		
	}
}
