using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace abutt1.ProcessedInput
{
	public class StandardInputProvider : IInputProvider {
		
		public SimpleTouch[] GetTouches ()
		{
			SimpleTouch[] ret = null;
			
			if(Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
			{
				ret = GetHandheldInput();
			}
			else
			{
				ret = GetMouseInput();
			}
		
			return ret;
		}
		
		// get mouse input
		SimpleTouch[] GetMouseInput()
		{
			List<SimpleTouch> ret = new List<SimpleTouch>();
			
			if(Input.GetMouseButton(0))   { ret.Add(TouchForMouseButton(0)); }
			if(Input.GetMouseButton(1))   { ret.Add(TouchForMouseButton(1)); }
			
			return ret.ToArray();
		}
		
		// get actual touch input
		SimpleTouch[] GetHandheldInput()
		{
			Touch[]         uTouches    = Input.touches;
			SimpleTouch[]   ret         = new SimpleTouch[uTouches.Length];
			
			for (int i = 0; i < ret.Length; i++) {
				ret[i] = new SimpleTouch(uTouches[i]);
			}
			
			return ret;
		}
		
		SimpleTouch TouchForMouseButton(int button)
		{
			SimpleTouch tmpTouch    = new SimpleTouch();
	
			tmpTouch.touchId        = button;
			tmpTouch.touchPos       = Input.mousePosition;
			
			if(Input.GetMouseButtonDown(button))    { tmpTouch.touchPhase = TouchPhase.Began;    }
			else if(Input.GetMouseButtonUp(button)) { tmpTouch.touchPhase = TouchPhase.Ended;    }
			else if(Input.GetMouseButton(button))   { tmpTouch.touchPhase = TouchPhase.Moved;    }
			else                                    { tmpTouch.touchPhase = TouchPhase.Canceled; }
			
			return tmpTouch;
		}
	}
}
