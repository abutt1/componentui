using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace abutt1.ProcessedInput
{
	public class StandardInputProviderBehaviour : MonoInputProvider {
		
		private IInputProvider providerImpl = new StandardInputProvider();
		
		public override SimpleTouch[] GetTouches ()
		{
			return providerImpl.GetTouches();
		}
	}
}
