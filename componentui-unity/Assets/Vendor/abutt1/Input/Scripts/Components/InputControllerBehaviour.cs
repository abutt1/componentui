using UnityEngine;
using System.Collections;

namespace abutt1.ProcessedInput
{
	public class InputControllerBehaviour : MonoBehaviour {
		
		public MonoInputProvider    InputProvider;
		public MonoInputModifier    InputModifier;
		public MonoInputParser      InputParser;
		
		// Use this for initialization
		void Start () 
		{
			RegisterComponentsToController();
		}
		
		// Update is called once per frame
		void Update () 
		{
			InputController.SharedInstance.CalculateTouches();
		}
		
		public void RegisterComponentsToController()
		{
			if(InputProvider != null) { InputController.SharedInstance.SetInputProvider(InputProvider); }
			if(InputModifier != null) { InputController.SharedInstance.SetInputModifier(InputModifier); }
			if(InputParser   != null) { InputController.SharedInstance.SetInputParser  (InputParser);   }
		}
		
#if abutt1_Debug
		void OnGUI()
		{
			Rect drawRect = new Rect(0,0,100,100);
			
			foreach (ITouch item in InputController.SharedInstance.ActiveTouches) {
				drawRect.center = item.CurrentPos;
				GUI.Box(drawRect, "" + item.TouchID);
				
				drawRect.center = item.InitialPos;
				GUI.Box(drawRect, "" + item.TouchID);
			}
		}
#endif
	}
}
