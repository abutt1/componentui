using UnityEngine;

namespace abutt1.Misc
{
	public struct FrameChecker {
		
		private int lastFrame;
		private int checkCounter;
		
		public int CheckFrame
		{
			get
			{
				IncrementCheckCount();
				return checkCounter;
			}
		}
		
		private void ResetCheckCount()      { checkCounter = 0; }
		private void IncrementCheckCount()  { 
			if(lastFrame != Time.frameCount)
			{
				ResetCheckCount();
				lastFrame = Time.frameCount;
			}
			checkCounter++;
		}
		
	}
}
