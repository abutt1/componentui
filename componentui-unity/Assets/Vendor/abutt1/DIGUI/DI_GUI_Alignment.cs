using UnityEngine;
using System.Collections;

namespace abutt1.DIGUI
{

	[System.Serializable]
	public class Alignment
	{
		public Rect 		RelativeRect	= new Rect(0,0,1,1);
		public Vector2 		Dimensions		= new Vector2(1,1);		// used in determining ratio
		public Vector2		Pivot			= new Vector2(0,0);		// where the image is positioned (relative)
		public RatioType 	RatioBehaviour	= RatioType.None;		// how to attempt to keep in ratio
		public Vector2		PixelOffset		= new Vector2(0,0);		// will be applied after calculating the rect
		public Vector2		RelativeOffset	= new Vector2(0,0);
		public Vector2		PixelCap		= new Vector2(0,0);		// max size the rect can be in pixels (no values mean no cap is applied).
	
		public Rect			GetCalculatedRect()
		{
			return GetCalculatedRect(Calculation.ScreenRect);
		}
		
		public Rect			GetCalculatedRect(Rect withinRect)
		{
			Rect ret = Calculation.CalculateDIRect(RelativeRect, withinRect, RatioBehaviour, GetXYRatio(), Pivot, PixelOffset, PixelCap);
			
			ret.x += ret.width	* RelativeOffset.x;
			ret.y += ret.height	* RelativeOffset.y;
			
			return ret;
		}
		
		public static implicit operator Rect(Alignment a)
		{
			return a.GetCalculatedRect();
		}

		public Alignment()
		{
		}
		
		public Alignment(Alignment other)
		{
			RelativeRect    = other.RelativeRect;
			Dimensions      = other.Dimensions;
			Pivot           = other.Pivot;
			RatioBehaviour  = other.RatioBehaviour;
			PixelOffset     = other.PixelOffset;
			RelativeOffset  = other.RelativeOffset;
			
			PixelCap        = other.PixelCap;
		}
			
		private float GetXYRatio()
		{
			return Dimensions.y != 0 ? Dimensions.x/Dimensions.y : 0;
		}
		
		#region Class Functions
		public static void Lerp(Alignment a, Alignment b,  Alignment toAffect, float t)
		{
			if(a != null && b != null && toAffect != null)
			{
				toAffect.RelativeRect	= Calculation.LerpRect(	a.RelativeRect,		b.RelativeRect,		t);
				toAffect.Dimensions		= Vector2.Lerp(			a.Dimensions,		b.Dimensions,		t);
				toAffect.Pivot			= Vector2.Lerp(			a.Pivot,			b.Pivot,			t);
				toAffect.RatioBehaviour = t < 0.5f ? 			a.RatioBehaviour : 	b.RatioBehaviour;	// necessary?
				toAffect.PixelOffset	= Vector2.Lerp(			a.PixelOffset,		b.PixelOffset,		t);
				toAffect.RelativeOffset	= Vector2.Lerp(			a.RelativeOffset,	b.RelativeOffset,	t);
				
				toAffect.PixelOffset	= Vector2.Lerp(			a.PixelCap,			b.PixelCap,			t);
			}
		}
		
		public static Alignment Lerp(Alignment a, Alignment b, float t)
		{
			Alignment ret = new Alignment();
			
			Lerp(a, b, ret, t);
			
			return ret;
		}		
		#endregion
	}
		
}
