namespace abutt1.DIGUI
{
	public enum RatioType
	{
		None,				// don't keep in ratio
		XScaleDominant,		// keep in ratio based on XScale
		YScaleDominant,		// keep in ratio based on YScale
		AspectFit,			// keep in ratio and fit
		AspectFill,			// keep in ratio and fill
	}
}
