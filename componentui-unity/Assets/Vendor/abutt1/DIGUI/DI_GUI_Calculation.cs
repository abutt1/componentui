using UnityEngine;
using System.Collections;

namespace abutt1.DIGUI
{

	/// <summary>
	/// Device Independent GUI System.
	/// A helper class to help with differing ratios.
	/// </summary>
	public class Calculation
	{	
		#region screenRes and Ratio
		
		public static int ScreenResX		{	get	{	return Screen.width;	}	}
		
		public static int ScreenResY		{	get	{	return Screen.height;	}	}
		
		/// <summary>
		/// get the screen ratio in terms of x (eg 640*480 -> 4:3)
		/// </summary>
		public static float ScreenRatioX	{	get	{	return (float)((float)ScreenResX / (float)ScreenResY);	}	}
	
		/// <summary>
		/// get the screen ratio in terms of y (eg 640*480 -> 3:4)
		/// </summary>
		public static float ScreenRatioY	{	get	{	return (float)((float)ScreenResY / (float)ScreenResX);	}	}
		
		public static Rect	ScreenRect		{	get	{	return new Rect(0,0,ScreenResX,ScreenResY);				}	}
		
		#endregion
		
		enum RectCalculationBehaviour
		{
			None,
			ScaleOnX,
			ScaleOnY,
		}
		
		#region calculateDIRect
		
		private static Rect PerformRectCalculation(	Rect withRect, Rect insideRect, 
													RatioType  withRatioType, float withXYRatio, 
													Vector2 withPivot, 
													Vector2 pixelOffset, 
													Vector2 pixelCap)
		{
			Rect ret = withRect;
			
			// ensure the pixel cap is always positive
			if(pixelCap.x < 0)	{	pixelCap.x *= -1;	}
			if(pixelCap.y < 0)	{	pixelCap.y *= -1;	}
			
			// if less than one, cap should be applied in comparison to rect
			if(pixelCap.x < 1)	{	pixelCap.x *= insideRect.width;		}
			if(pixelCap.y < 1)	{	pixelCap.y *= insideRect.height;	}

			// ensure the ratio is positive
			withXYRatio = Mathf.Abs(withXYRatio);
			
			bool flipWidth	= withRect.width	< 0;
			bool flipHeight	= withRect.height	< 0;
			
			// work out how we should calculate the rect
			RectCalculationBehaviour calcBehaviour = RectCalculationBehaviour.None;
			
			#region PerformRectCalculation.DetermineBehaviour
			switch(withRatioType)
			{
				case(RatioType.None):			{	calcBehaviour = RectCalculationBehaviour.None;		break;	}
				case(RatioType.XScaleDominant):	{	calcBehaviour = RectCalculationBehaviour.ScaleOnX;	break;	}
				case(RatioType.YScaleDominant):	{	calcBehaviour = RectCalculationBehaviour.ScaleOnY;	break;	}
				//to implement
				case(RatioType.AspectFit):		{	
					
					// only going to care about being relative to insideRect. so ensure that they are
					withRect.width	= withRect.width	< 0 ? -1f : 1f;
					withRect.height	= withRect.height	< 0 ? -1f : 1f;
				
					pixelCap.x		= 0f;	pixelCap.y		= 0f;
					
					if(ShouldScaleBasedOnX(true, (insideRect.width/insideRect.height), withXYRatio))
					{
						calcBehaviour = RectCalculationBehaviour.ScaleOnX;
					}
					else
					{
						calcBehaviour = RectCalculationBehaviour.ScaleOnY;
					}
					break;
				}
				case(RatioType.AspectFill):		{	
					
					// only going to care about being relative to insideRect. so ensure that they are
					withRect.width	= withRect.width	< 0 ? -1f : 1f;
					withRect.height	= withRect.height	< 0 ? -1f : 1f;
				
					pixelCap.x		= 0f;	pixelCap.y		= 0f;
					
					if(ShouldScaleBasedOnX(false, (insideRect.width/insideRect.height), withXYRatio))
					{
						calcBehaviour = RectCalculationBehaviour.ScaleOnX;
					}
					else
					{
						calcBehaviour = RectCalculationBehaviour.ScaleOnY;
					}
					break;
				}
			}
			#endregion
			
			// calculate the rect
			#region PerformRectCalculation.Calculation
			switch(calcBehaviour)
			{
				#region PerformRectCalculation.Calculation.None
				case(RectCalculationBehaviour.None):
				{
					// make a relative rect
					ret.width 	= insideRect.width	* withRect.width;
					ret.height	= insideRect.height	* withRect.height;
			
					// cap if necessary
					if( ShouldCapDimension(ret.width, pixelCap.x) )	{
						ret.width = CapDimension(ret.width, pixelCap.x);
					}
					if( ShouldCapDimension(ret.height, pixelCap.y) ) {
						ret.height = CapDimension(ret.height, pixelCap.y);
					}
					break;
				}
				#endregion
				#region PerformRectCalculation.Calculation.ScaleOnX
				case(RectCalculationBehaviour.ScaleOnX):
				{	// make a relative rect
					ret.width 	= insideRect.width	* withRect.width;

					// cap if necessary
					ret.width	= CapDimension(ret.width, pixelCap.x);
					
					// keep in ratio
					ret.height	= CalculateHeight(ret.width, withXYRatio, flipHeight);
					
					// check if its not too big -> if it is, check to see if we'll have to rescale it
					if( ShouldCapDimension(ret.height, pixelCap.y) ) {
						ret.height	= CapDimension(ret.height, pixelCap.y);
						ret.width	= CalculateWidth(ret.height, withXYRatio, flipWidth);
					}
					break;
				}
				#endregion
				#region PerformRectCalculation.Calculation.ScaleOnY
				case(RectCalculationBehaviour.ScaleOnY):
				{
					// make a relative rect
					ret.height	= insideRect.height	* withRect.height;
					
					// cap if necessary
					ret.height = CapDimension(ret.height, pixelCap.y);

					// keep in ratio
					ret.width	= CalculateWidth(ret.height, withXYRatio, flipWidth);
				
					// check if its not too big -> if it is, check to see if we'll have to rescale it
					if( ShouldCapDimension(ret.width, pixelCap.x) )	{
						ret.width	= CapDimension(ret.width, pixelCap.x);
						ret.height	= CalculateHeight(ret.width, withXYRatio, flipHeight);
					}
					break;
				}
				#endregion
			}
			#endregion
	
			// calculate x and y position
			ret.x 	= insideRect.width	* withRect.x + insideRect.x;
			ret.y 	= insideRect.height	* withRect.y + insideRect.y;
			
			// offset on pivot
			ret.x -= ret.width 	* withPivot.x; 
			ret.y -= ret.height	* withPivot.y;
						
			// apply the final offset
			ret.x += pixelOffset.x;
			ret.y += pixelOffset.y;
			
			return ret;
			
			//thought process..
			
//			// make a relative rect
//			ret.width 	= insideRect.width	* withRect.width;
//			ret.height	= insideRect.height	* withRect.height;
//	
//			// cap if necessary
//			if(pixelCap.x > 0 && pixelCap.x < ret.width)	{
//				ret.width = pixelCap.x;
//			}
//			if(pixelCap.y > 0 && pixelCap.y < ret.height)	{
//				ret.height = pixelCap.y;
//			}
//			
//
//			// here we do the x or y ratio stuff
////			//if xratio
////			{
////				ret.height	= CalculateDimension(false, ret.width, ret.height, withXYRatio);
////			}
////			//else if y ratio
////			{
////				ret.width	= CalculateDimension(true, ret.width, ret.height, withXYRatio);
////			}
//			
//			// then...
//			
//			// calculate x and y position (after everything else
//			ret.x 		= insideRect.width	* withRect.x + insideRect.x;
//			ret.y 		= insideRect.height	* withRect.y + insideRect.y;
//			
//			// offset on pivot
//			ret.x -= ret.width 	* withPivot.x; 
//			ret.y -= ret.height	* withPivot.y;
//						
//
//			// apply the final offset
//			ret.x += pixelOffset.x;
//			ret.y += pixelOffset.y;
//			
//			return ret;
			
		}
	
		#region Interface
			
		// with insideRect
		#region CalculateDIRectCalls.insideRect
		public static Rect CalculateDIRect(	Rect withRect, 
											Rect insideRect, 
											RatioType  withRatioType, 
											float withXYRatio, 
											Vector2 withPivot, 
											Vector2 pixelOffset, 
											Vector2 pixelCap )
		{
			return PerformRectCalculation(withRect, insideRect, withRatioType, withXYRatio, withPivot, pixelOffset, pixelCap);
		}
		
		public static Rect CalculateDIRect(	Rect withRect, 
											Rect insideRect, 
											RatioType  withRatioType, 
											float withXYRatio, 
											Vector2 withPivot, 
											Vector2 pixelOffset
											)
		{
			return PerformRectCalculation(withRect, insideRect, withRatioType, withXYRatio, withPivot, pixelOffset, new Vector2(0,0));
		}

		public static Rect CalculateDIRect(	Rect withRect, 
											Rect insideRect, 
											RatioType  withRatioType, 
											float withXYRatio, 
											Vector2 withPivot
											)
		{
			return PerformRectCalculation(withRect, insideRect, withRatioType, withXYRatio, withPivot, new Vector2(0,0), new Vector2(0,0));
		}
		
		public static Rect CalculateDIRect(	Rect withRect, 
											Rect insideRect, 
											RatioType  withRatioType, 
											float withXYRatio
											)
		{
			return PerformRectCalculation(withRect, insideRect, withRatioType, withXYRatio, new Vector2(0,0), new Vector2(0,0), new Vector2(0,0));
		}
		#endregion

		// without inside
		#region CalculateDIRectCalls.withRect
		public static Rect CalculateDIRect(	Rect withRect, 
											RatioType  withRatioType, 
											float withXYRatio, 
											Vector2 withPivot, 
											Vector2 pixelOffset, 
											Vector2 pixelCap )
		{
			return PerformRectCalculation(withRect, new Rect(0,0,ScreenResX, ScreenResY), withRatioType, withXYRatio, withPivot, pixelOffset, pixelCap);
		}
		
		public static Rect CalculateDIRect(	Rect withRect, 
											RatioType  withRatioType, 
											float withXYRatio, 
											Vector2 withPivot, 
											Vector2 pixelOffset
											)
		{
			return PerformRectCalculation(withRect, new Rect(0,0,ScreenResX, ScreenResY), withRatioType, withXYRatio, withPivot, pixelOffset, new Vector2(0,0));
		}

		public static Rect CalculateDIRect(	Rect withRect, 
											RatioType  withRatioType, 
											float withXYRatio, 
											Vector2 withPivot
											)
		{
			return PerformRectCalculation(withRect, new Rect(0,0,ScreenResX, ScreenResY), withRatioType, withXYRatio, withPivot, new Vector2(0,0), new Vector2(0,0));
		}
		
		public static Rect CalculateDIRect(	Rect withRect, 
											RatioType  withRatioType, 
											float withXYRatio
											)
		{
			return PerformRectCalculation(withRect, new Rect(0,0,ScreenResX, ScreenResY), withRatioType, withXYRatio, new Vector2(0,0), new Vector2(0,0), new Vector2(0,0));
		}
		#endregion
		
		public static Rect LerpRect(Rect fromRect, Rect toRect, float t)
		{
			fromRect.x      =	Mathf.Lerp(fromRect.x,		toRect.x,		t);
			fromRect.y      =	Mathf.Lerp(fromRect.y,		toRect.y,		t);
			fromRect.width  =	Mathf.Lerp(fromRect.width,	toRect.width,	t);
			fromRect.height =	Mathf.Lerp(fromRect.height,	toRect.height,	t);
			
			return fromRect;
		}
		
		public static Rect MoveTowardsRect(Rect fromRect, Rect toRect, float maxStep, bool clamp = true)
		{
			float newX = MoveTowardsFloat(fromRect.x,      toRect.x,      maxStep, clamp);
			float newY = MoveTowardsFloat(fromRect.y,      toRect.y,      maxStep, clamp);
			float newW = MoveTowardsFloat(fromRect.width,  toRect.width,  maxStep, clamp);
			float newH = MoveTowardsFloat(fromRect.height, toRect.height, maxStep, clamp);
			
			return new Rect(newX, newY, newW, newH);
		}
		
		private static float MoveTowardsFloat(float fromF, float toF, float maxStep, bool clamp)
		{
			if(fromF == toF)
			{
				return fromF;
			}
			else
			{
				if(fromF < toF)
				{
					fromF += maxStep;
					if(clamp && fromF > toF)
					{
						fromF = toF;
					}
					return fromF;
				}
				else
				{
					fromF -= maxStep;
					if(clamp && fromF < toF)
					{
						fromF = toF;
					}
					return fromF;					
				}
			}
		}
		
		Rect CalculateRelativeRect(Rect fromSourceRect, Vector2 dimensions)
		{
			fromSourceRect.x        /= dimensions.x;
			fromSourceRect.y        /= dimensions.y;
			fromSourceRect.width    /= dimensions.x;
			fromSourceRect.height   /= dimensions.y;
			
			return fromSourceRect;
		}
		
		public static Rect RoundRectToInt(Rect toRound)
		{
			toRound.x		= Mathf.RoundToInt(toRound.x);
			toRound.y		= Mathf.RoundToInt(toRound.y);
			toRound.width	= Mathf.RoundToInt(toRound.width);
			toRound.height	= Mathf.RoundToInt(toRound.height);
			return toRound;
		}
		
		#endregion
		
		#region CalculationHelpers
		
		/// <summary>
		/// Determines whether or not an element should scale based on x.
		/// </summary>
		/// <returns>
		/// Returns true if operation element should be scaled based on x.
		/// </returns>
		/// <param name='fit'>
		/// If set to <c>true</c> fit, if set to <c>false</c> fill.
		/// </param>
		/// <param name='containerRatio'>
		/// The container rect's ratio.
		/// </param>
		/// <param name='elementRatio'>
		/// The element rect's ratio.
		/// </param>
		static bool ShouldScaleBasedOnX(bool fit, float containerRatio, float elementRatio)
		{
			//bool ret = (elementRatio > containerRatio);
			bool ret = (Mathf.Abs(elementRatio) > Mathf.Abs(containerRatio));
		
			// inverse if it should fill
			if(!fit)	{	ret = !ret;	}
			
			return ret;
		}
		
		/// <summary>
		/// Calculates the width or height using an X:Y ratio
		/// </summary>
		/// <returns>
		/// The dimension.
		/// </returns>
		/// <param name='returnWidth'>
		/// If set to <c>true</c> will return width, if set to <c>false</c> will return height.
		/// </param>
		/// <param name='currentWidth'>
		/// Current width.
		/// </param>
		/// <param name='currentHeight'>
		/// Current height.
		/// </param>
		/// <param name='withXYRatio'>
		/// The X:Y ratio.
		/// </param>
		static float CalculateDimension(bool returnWidth, float currentWidth, float currentHeight, float withXYRatio)
		{
			if(withXYRatio != 0)
			{
				if(returnWidth)
				{	// return new width val
					return (currentHeight * withXYRatio);
				}
				else
				{	// return new height val
					return (currentWidth / withXYRatio);
				}
			}
			
			return 0f;
		}
		
		static float CalculateWidth(float currentHeight, float withXYRatio, bool returnNegativeValue = false)
		{
			float ret = CalculateDimension(true, 0, Mathf.Abs(currentHeight),	withXYRatio);
			return returnNegativeValue ? -ret : ret;
		}
		
		static float CalculateHeight(float currentWidth, float withXYRatio, bool returnNegativeValue = false)
		{
			float ret = CalculateDimension(false, Mathf.Abs(currentWidth), 0,	withXYRatio);
			return returnNegativeValue ? -ret : ret;
		}
		
		
		/// <summary>
		/// Determines whether a given dimension with a given cap limit should be capped.
		/// NOTE: cap is assumed to be positive
		/// </summary>
		/// <returns>
		/// Whether or not the dimension should be capped.
		/// </returns>
		static bool ShouldCapDimension(float dimension, float cap)
		{
			if(cap > 0 && cap < Mathf.Abs(dimension))
			{
				return true;
			}
			return false;
		}

		/// <summary>
		/// Caps the dimension.
		/// </summary>
		/// <returns>
		/// The capped dimension.
		/// </returns>
		/// <param name='dimension'>
		/// Dimension.
		/// </param>
		/// <param name='cap'>
		/// Cap.
		/// </param>
		static float CapDimension(float dimension, float cap)
		{
			cap = Mathf.Abs(cap);
			
			if(ShouldCapDimension(dimension, cap))
			{
				return dimension > 0 ? cap : -cap;
			}
			
			return dimension;
		}
				
		#endregion
		
		#endregion
		
	}
	
}