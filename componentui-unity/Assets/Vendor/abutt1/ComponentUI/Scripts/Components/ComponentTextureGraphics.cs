using UnityEngine;
using System.Collections;

namespace abutt1.ComponentUI
{
	[RequireComponent (typeof(ComponentDrawer))]
	public class ComponentTextureGraphics : BasicRenderable {

		public  Texture     Tex;
		public  Rect        SourceRect = new Rect(0,0,1,1);
		public  Material    Mat;
		public  RectOffset  Border;
		
		public override void Draw (Rect inRect)
		{
			if(Tex == null) { return; }
			
			// don't need to set up a pixel matrix here -> as unity will have set it up for us
			Graphics.DrawTexture(inRect, Tex, SourceRect, Border.left, Border.right, Border.top, Border.bottom, ModTexInputColor(GUI.color), Mat);
		}
	
		Color ModTexInputColor(Color col)
		{
			Color ret = col;
			ret.r *= 0.5f;
			ret.b *= 0.5f;
			ret.g *= 0.5f;
			ret.a *= 0.5f;
			return ret;
		}
		
	}
}
