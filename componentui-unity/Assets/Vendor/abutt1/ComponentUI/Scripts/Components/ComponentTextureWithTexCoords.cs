using UnityEngine;
using System.Collections;

namespace abutt1.ComponentUI
{
	[RequireComponent (typeof(ComponentDrawer))]
	public class ComponentTextureWithTexCoords : BasicRenderable {

		public  Texture     Tex;
		public  Rect        SourceRect = new Rect(0,0,1,1);
		
		public override void Draw (Rect inRect)
		{
			if(Tex == null) { return; }
			
			GUI.DrawTextureWithTexCoords(inRect, Tex, SourceRect);
		}
		
	}
}
