using UnityEngine;
using System.Collections;

namespace abutt1.ComponentUI
{
	[RequireComponent (typeof(ComponentDrawer))]
	public class ComponentButtonRenderer : BasicInputRenderer {
		
		public  ButtonTarget  Target;
		
		public override void OnTouchUp (Vector2 atPos, bool onReceiver)
		{
			if(!onReceiver) { return; }
			
			if(Target != null)
			{
				Target.SendMessageIfAble();
			}
		}
		
	}
}
