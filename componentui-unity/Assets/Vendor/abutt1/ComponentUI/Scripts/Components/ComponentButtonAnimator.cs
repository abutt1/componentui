using UnityEngine;
using System.Collections;

namespace abutt1.ComponentUI
{
	[RequireComponent (typeof(ComponentDrawer))]
	public class ComponentButtonAnimator : BasicInputRenderer {
		
		public  ButtonTarget    Target;
		
		public  ComponentDrawer DrawerToAnimate;
				
		#region Input
		
		public override void OnTouchUp (Vector2 atPos, bool onReceiver)
		{
			base.OnTouchUp(atPos, onReceiver);
			
			if(onReceiver && Target != null)
			{
				Target.SendMessageIfAble();
			}
			
			// finish anim
		}
		
		public override void OnTouch (Vector2 atScreenPos, bool onReceiver, Vector2 wDelta)
		{
			base.OnTouch (atScreenPos, onReceiver, wDelta);
			
			// do hover
		}
		
		public override void OnTouchDown (Vector2 atScreenPos)
		{
			base.OnTouchDown (atScreenPos);
			
			// do start anim
		}
		
		#endregion
		
	}
}
