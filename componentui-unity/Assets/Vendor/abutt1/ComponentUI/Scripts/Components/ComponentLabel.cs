using UnityEngine;
using System.Collections;

namespace abutt1.ComponentUI
{
	[RequireComponent (typeof(ComponentDrawer))]
	public class ComponentLabel : BasicRenderable {
		
		public  CustomSkinStyleContainer    StyleInfo;
		public  string                      Text;
		
		public override void Draw (Rect inRect)
		{
			GUI.Label(inRect, Text, StyleInfo.StyleSafe);
		}
		
	}
}
