using UnityEngine;
using System.Collections;

namespace abutt1.ComponentUI
{
	public class BasicRenderable : MonoBehaviour, IRenderable
	{
		public  virtual void    Draw    (Rect inRect) { }
		public  virtual void    Layout  (Rect inRect) { }
	}
	
	public class BasicTouchReceiver : MonoBehaviour, ITouchReceiver
	{
		public  virtual void    OnTouchDown(    Vector2 atScreenPos)                                    {}
		public  virtual void    OnTouch(        Vector2 atScreenPos, bool onReceiver, Vector2 wDelta)   {}
		public  virtual void    OnTouchUp(      Vector2 atScreenPos, bool onReceiver)                   {}
	}
	
	public class BasicInputRenderer : BasicRenderable, ITouchReceiver
	{
		private bool    m_ControlActive;
		private Vector2 m_InitialScreenPos;
		private Vector2 m_CurrentScreenPos;
		private Vector2 m_CurrentDelta;
		
		protected bool    ControlActive     { get { return m_ControlActive;                                 } }
		protected Vector2 InitialScreenPos  { get { return m_InitialScreenPos;                              } }
		protected Vector2 InitialGUIPos     { get { return GUIUtility.ScreenToGUIPoint(InitialScreenPos);   } }
		protected Vector2 CurrentScreenPos  { get { return m_CurrentScreenPos;                              } }
		protected Vector2 CurrentGUIPos     { get { return GUIUtility.ScreenToGUIPoint(CurrentScreenPos);   } }
		protected Vector2 CurrentDelta      { get { return m_CurrentDelta;                                  } }
		
		public  virtual void    OnTouchDown(    Vector2 atScreenPos)
		{
			m_ControlActive     = true;
			m_InitialScreenPos  = atScreenPos;
			m_CurrentScreenPos  = atScreenPos;
			m_CurrentDelta      = Vector2.zero;
		}
		public  virtual void    OnTouch(        Vector2 atScreenPos, bool onReceiver, Vector2 wDelta)
		{
			m_CurrentScreenPos  = atScreenPos;
			m_CurrentDelta      = wDelta;
		}
		public  virtual void    OnTouchUp(      Vector2 atScreenPos, bool onReceiver)
		{
			m_ControlActive     = false;
			m_CurrentDelta      = atScreenPos - m_CurrentScreenPos;
			m_CurrentScreenPos  = atScreenPos;
		}
	}
}
