using UnityEngine;
using System.Collections;

namespace abutt1.ComponentUI
{
	[RequireComponent (typeof(ComponentDrawer))]
	public class ComponentTexture : BasicRenderable {

		public  Texture     Tex;
		
		public override void Draw (Rect inRect)
		{
			if(Tex == null) { return; }
			
			GUI.DrawTexture(inRect, Tex);
		}
		
	}
}
