using UnityEngine;
using System.Collections;
using abutt1.DIGUI;

namespace abutt1.ComponentUI
{
	[RequireComponent (typeof(ComponentDrawer))]
	public class ExampleBox : BasicRenderable {
		
		public override void Draw (Rect inRect)
		{
			GUI.Box(inRect, "");
		}
				
	}
}
