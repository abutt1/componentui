using UnityEngine;
using System.Collections;
using abutt1.DIGUI;

namespace abutt1.ComponentUI
{
	[RequireComponent (typeof(ComponentDrawer))]
	public class ExampleInputVisualiser : BasicInputRenderer {
		
		public override void Draw (Rect inRect)
		{
			GUI.Box(inRect, "");
			
			if(ControlActive)
			{
				Vector2 testCenter = CurrentGUIPos;
				
				// control the look of our boxes.
				float tmpDimensionA = Mathf.Min(Screen.height, Screen.width) * .01f;
				float tmpDimensionB = tmpDimensionA * 10;
				
				float tmpIntensity  =  tmpDimensionA * CurrentDelta.magnitude;
				tmpDimensionB       += tmpIntensity;
				
				Rect drawRect;
				drawRect = new Rect(0,0,tmpDimensionA,tmpDimensionB);
				drawRect.center = testCenter;
				GUI.Box(drawRect, "");
				
				drawRect = new Rect(0,0,tmpDimensionB,tmpDimensionA);
				drawRect.center = testCenter;
				GUI.Box(drawRect, "");
			}
		}
		
		public override void OnTouchDown (Vector2 atPos)
		{
			base.OnTouchDown(atPos);
		}
				
	}
}
