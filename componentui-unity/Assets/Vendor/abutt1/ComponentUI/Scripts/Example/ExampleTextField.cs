using UnityEngine;
using System.Collections;
using abutt1.DIGUI;

namespace abutt1.ComponentUI
{
	[RequireComponent (typeof(ComponentDrawer))]
	public class ExampleTextField : BasicInputRenderer {
		
		public  SkinStyleContainer  ActiveStyle;
		public  SkinStyleContainer  InactiveStyle;
		
		public  string  Text;
		
		private bool    TextFieldActive { get { return UIInputManager.LastFocusedControl == this; } }
		
		public override void Draw (Rect inRect)
		{
			if(TextFieldActive)
			{
				// draw with an active style
				GUI.Label(inRect, Text, ActiveStyle.StyleSafe);
			}
			else
			{
				// draw with an inactive style
				GUI.Label(inRect, Text, InactiveStyle.StyleSafe);
			}
		}
		
		public override void OnTouchDown (Vector2 atPos)
		{
			base.OnTouchDown(atPos);
			
			StartUpTextField();
		}
	
		void Update()
		{
			if(TextFieldActive)
			{
				ParseAndApplyInputString(Input.inputString);
			}
		}
		
		void ParseAndApplyInputString(string inputString)
		{
			foreach (char c in inputString) {
	
				if (c == '\b')
				{
					// backspace
					if(!string.IsNullOrEmpty(Text))
					{
						Text = Text.Substring(0, Text.Length -1);
					}
				}
				else if(c == '\n' || c == '\r')
				{
					// return
					WrapUpTextField();
				}
				else
				{
					Text += c;
				}
				
			}
			
		}
		
		void StartUpTextField()
		{
			// put up the keyboard if required...
		}
		
		void WrapUpTextField()
		{
			UIInputManager.UnfocusLastControl();
		}
		
	}
}
