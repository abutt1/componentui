using UnityEngine;
using System.Collections;
using abutt1.DIGUI;

namespace abutt1.ComponentUI
{
	public class ExampleColorChanger : MonoBehaviour {
		
		public  ComponentDrawer     Drawer;
		
		public  Color               ColorA;
		public  Color               ColorB;

		public  float               ChangeDuration = 1;
		private float               changeTimer;

		void Start()
		{
			if(Drawer == null) { Drawer = GetComponent<ComponentDrawer>(); }
		}
		
		void Update()
		{
			changeTimer         += Time.deltaTime;
			
			float tmpDuration   =  ChangeDuration > 0 ? ChangeDuration : 1;
			
			if(changeTimer > tmpDuration)
			{
				changeTimer -= tmpDuration;
			}
			
			float tmpLerpVal    =  changeTimer / tmpDuration;
			
			Color tmpColor = Color.Lerp(ColorA, ColorB, tmpLerpVal);
			
			if(Drawer != null)
			{
				Drawer.Instructions.Color = tmpColor;
			}
		}
		
	}
}
