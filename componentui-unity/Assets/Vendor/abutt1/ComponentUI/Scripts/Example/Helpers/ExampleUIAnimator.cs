using UnityEngine;
using System.Collections;
using abutt1.DIGUI;

namespace abutt1.ComponentUI
{
	[RequireComponent (typeof(Animation))]
	public class ExampleUIAnimator : MonoBehaviour {
		
		public enum MovementType
		{
			Default = 0,
			Scale,
		}
		
		public  MovementType        MovementBehaviour;
		public  ComponentDrawer     Drawer;
		public  bool                OnlyApplyMatrixIfAnimating;
		
		void Start()
		{
			if(Drawer == null) { Drawer = GetComponent<ComponentDrawer>(); }
		}
		
		void Update()
		{
			if(Drawer == null) { return; }
			
			DrawInstructions instructions = Drawer.Instructions;
			
			Vector3 tPos            = transform.localPosition;
			Vector3 tRot            = transform.localRotation.eulerAngles;
			Vector3 tScale          = transform.localScale;
			
			Rect    newRelativeRect = instructions.Alignment.RelativeRect;
			int     newDepth;
			float   newRotation;
			
			// grab the x,y position from our position
			newRelativeRect.x = tPos.x;
			newRelativeRect.y = tPos.y;
			
			// set w&h if required 
			if(MovementBehaviour == MovementType.Default)
			{
				newRelativeRect.width  = tScale.x;
				newRelativeRect.height = tScale.y;
			}
			
			// grab the depth from our z
			newDepth          = (int)tPos.z;
			
			// rotation from the z
			newRotation       = tRot.z;
			
			instructions.Alignment.RelativeRect = newRelativeRect;
			instructions.Depth                  = newDepth;
			instructions.Rotation               = newRotation;
			
			// only adjust scale if required
			if(MovementBehaviour == MovementType.Scale)
			{
				instructions.Scale                  = new Vector2(tScale.x, tScale.y);
			}
			
			if(OnlyApplyMatrixIfAnimating)
			{
				instructions.ApplyMatrix = animation.isPlaying;
			}
		}
		
	}
}
