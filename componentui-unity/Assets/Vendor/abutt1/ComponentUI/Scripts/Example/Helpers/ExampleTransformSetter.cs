using UnityEngine;
using System.Collections;
using abutt1.DIGUI;

namespace abutt1.ComponentUI
{
	public class ExampleTransformSetter : MonoBehaviour {
		
		public  ComponentDrawer     Drawer;
		
		void Start()
		{
			if(Drawer == null) { Drawer = GetComponent<ComponentDrawer>(); }
		}
		
		void Update()
		{
			if(Drawer == null) { return; }
			
			DrawInstructions instructions = Drawer.Instructions;
			
			Vector3 tPos            = transform.localPosition;
			Vector3 tRot            = transform.localRotation.eulerAngles;
			Vector3 tScale          = transform.localScale;
			
			Rect    newRelativeRect = new Rect();
			int     newDepth;
			float   newRotation;
			
			// grab the x,y position from our position
			newRelativeRect.x = tPos.x;
			newRelativeRect.y = tPos.y;
			
			// grab the w,h from our scale
			newRelativeRect.width   = tScale.x;
			newRelativeRect.height  = tScale.y;
			
			// grab the depth from our z
			newDepth          = (int)tPos.z;
			
			// rotation from the z
			newRotation       = tRot.z;
			
			// scale from what?
			
			
			instructions.Alignment.RelativeRect = newRelativeRect;
			instructions.Depth                  = newDepth;
			instructions.Rotation               = newRotation;
		}
		
	}
}
