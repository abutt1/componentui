using UnityEngine;
using System.Collections;

namespace abutt1.ComponentUI
{
	public interface ITouchReceiver 
	{
		void    OnTouchDown(    Vector2 atScreenPos);
		void    OnTouch(        Vector2 atScreenPos, bool onReceiver, Vector2 wDelta);
		void    OnTouchUp(      Vector2 atScreenPos, bool onReceiver);
	}
}
