using UnityEngine;
using System.Collections;

namespace abutt1.ComponentUI
{
	public interface IRenderable {
	
		void Draw(Rect inRect);
		void Layout(Rect inRect);
	}
}
