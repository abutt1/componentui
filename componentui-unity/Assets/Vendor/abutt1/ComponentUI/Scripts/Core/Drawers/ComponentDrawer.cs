using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// Thoughts: 
//			// position -> x&y for x and y... z for ?
//			
//			// rotation -> look at quaternions perhaps. worst case only need one variable
//			// scale    -> should refer to scale


namespace abutt1.ComponentUI
{
	public class ComponentDrawer : MonoBehaviour, ITouchReceiver {

		#region Fields
				
		public  DrawInstructions            Instructions = new DrawInstructions();
				
		#region CachedVals
		
		private ComponentDrawer             cachedParent;
		
		private ComponentDrawer[]           cachedChildren;
		private IRenderable[]               cachedRenderers;
		private ITouchReceiver[]            cachedInputReceivers;
		
		private Rect                        cachedLiteralRect;
		private Rect                        cachedGroupRect;
		private Rect                        cachedDrawRect;
		
		#region CachedGUIState
			
		private Color                       cachedGUIPrevCol;
		private Color                       cachedGUIPrevFGCol;
		private Color                       cachedGUIPrevBGCol;
		private Matrix4x4                   cachedGUIPrevMatrix;
		
		private Color                       cachedGUIDrawCol;
		private Color                       cachedGUIDrawFGCol;
		private Color                       cachedGUIDrawBGCol;
		private Matrix4x4                   cachedGUIDrawMatrix;
		
		#endregion
		
		#endregion
		
		#region HelperProperties
		private ComponentDrawer             ParentDrawer
		{
			get
			{
				Cache_Parent();
				return cachedParent;
			}
		}
		
		private bool                        HasElementsToDraw
		{
			get
			{
				bool noRenderersAttached    = (cachedRenderers == null || cachedRenderers.Length == 0);
				bool noDrawerChildren       = (cachedChildren == null  || cachedChildren.Length  == 0);
				return !(noRenderersAttached && noDrawerChildren);
			}
		}
		
		private bool                        InputValid
		{
			get
			{
				if( Instructions.ApplyMatrix )                                  { return false; }
				if( AffectedByParentMatrix )                                    { return false; }				
				return true;
			}
		}
		
		private bool                        AffectedByParentMatrix
		{
			get
			{
				if( cachedParent != null)
				{
					if( cachedParent.Instructions.ApplyMatrix )                 { return true; }
					
					return cachedParent.AffectedByParentMatrix;
				}
				return false;
			}
		}
		
		private bool                        InGroup
		{
			get
			{
				if( cachedParent != null)
				{
					if( cachedParent.Instructions.Group )                       { return true; }
					
					return cachedParent.InGroup;
				}
				return false;
			}
		}
		
		private Rect                        ContainerRect
		{
			get
			{
				if(cachedParent != null) 
				{
					return cachedParent.cachedDrawRect;
				}
				return new Rect(0,0,Screen.width, Screen.height);
			}
		}
		
		private Rect                        ContainerRectLiteral
		{
			get
			{
				if(cachedParent != null) 
				{
					return cachedParent.cachedLiteralRect;
				}
				return ContainerRect;
			}
		}
		
		private Rect                        InputRect
		{
			get
			{
				Rect tmpInputRect = cachedLiteralRect;
				
				ComponentDrawer tmpParent = cachedParent;
				while(tmpParent != null)
				{
					if(tmpParent.Instructions.Group)
					{
						tmpInputRect = tmpParent.cachedLiteralRect;
						break;
					}
					
					tmpParent = tmpParent.cachedParent;
				}

				float tmpX      = tmpInputRect.x;
				float tmpY      = tmpInputRect.y;
				float tmpXMax   = tmpInputRect.xMax;
				float tmpYMax   = tmpInputRect.yMax;
				
				float newX      = Mathf.Clamp(cachedLiteralRect.x,    tmpX, tmpXMax);
				float newY      = Mathf.Clamp(cachedLiteralRect.y,    tmpY, tmpYMax);
				float newXMax   = Mathf.Clamp(cachedLiteralRect.xMax, tmpX, tmpXMax);
				float newYMax   = Mathf.Clamp(cachedLiteralRect.yMax, tmpY, tmpYMax);
				
				return new Rect(newX, newY, newXMax-newX, newYMax-newY);
			}
		}
		
		#endregion
		
		#endregion
		
		#region Interface
		
		public void Refresh()
		{
			RefreshGUIElements();
		}
		
		#endregion
		
		#region Mono
		
		void Awake()
		{
			// grab all attached guis
			RefreshGUIElements();
		}
				
		void OnGUI()
		{
			if(ParentDrawer == null)
			{
				// only let the parent set depth -> (we should use the children's depth in sorting their draw order)
				GUI.depth = Instructions.Depth;
				GUIDrawLogic();
			}
		}
		
		#endregion
		
		#region Logic
		
		void GUIDrawLogic()
		{
			if(!enabled || !gameObject.activeInHierarchy)                                           { return; }
			if(Event.current.type != EventType.Layout && Event.current.type != EventType.Repaint)   { return; } // don't bother if not a layout or repaint event
			if(!HasElementsToDraw)                                                                  { return; }
			
			if(         Event.current.type == EventType.Layout)
			{
				// we prepare the gui in the layout stage, and wrap up in the next frame's update
				CacheGUIState();				
			}
			
			// perform any necessary start-up operations (such as groups)
			StartUpGUI();
			
			if(         Event.current.type == EventType.Layout)
			{
				#region Layout
				
				// tell our attached renderers to layout
				if(cachedRenderers != null)
				{
					for (int i = 0; i < cachedRenderers.Length; i++) 
					{
						IRenderable tmp = cachedRenderers[i];
						if(tmp != null) 
						{
							tmp.Layout(cachedDrawRect);
						}
					}
					// reset the state (potentially don't need to worry about color if not repaint)
					ApplyGUIState();
				}
				
				
				#endregion
			}
			else if(    Event.current.type == EventType.Repaint)
			{
				// set up our input, (as now unity depth will be sorted)
				if(cachedInputReceivers != null && cachedInputReceivers.Length > 0 && InputValid)
				{
					RegisterInput();
				}
				
				#region Repaint
				
				ApplyGUIState();
				
				for (int i = 0; i < cachedRenderers.Length; i++) 
				{
					IRenderable tmp = cachedRenderers[i];
					if(tmp != null)
					{
						tmp.Draw(cachedDrawRect);
					}
					
					// reset the state (in case any of our renderers have altered it).
					ApplyGUIState();
				}
				
				#endregion
				
			}
			
			
			// AFTER drawing our stuff, draw the children
			#region ChildrenDrawers
			if(cachedChildren != null)
			{
				for (int i = 0; i < cachedChildren.Length; i++) 
				{
					ComponentDrawer tmp = cachedChildren[i];
					if(tmp != null)
					{
						tmp.GUIDrawLogic();
					}
				}
			}
			#endregion

			// wrap up previous frame's operations
			WrapUpGUI();
			
			
		}
		
		#region GUI preparation
		void CacheGUIState()
		{
			// cache our draw rect
			cachedLiteralRect   = Instructions.Alignment.GetCalculatedRect(ContainerRectLiteral);
			cachedDrawRect      = Instructions.Alignment.GetCalculatedRect(ContainerRect);
			
			
			// cache gui state
			cachedGUIPrevCol    = GUI.color;
			cachedGUIPrevFGCol  = GUI.contentColor;
			cachedGUIPrevBGCol  = GUI.backgroundColor;
			cachedGUIPrevMatrix = GUI.matrix;
			
			// work out our new draw state
			cachedGUIDrawCol    = cachedGUIPrevCol      * Instructions.Color;
			cachedGUIDrawFGCol  = cachedGUIPrevFGCol    * Instructions.FGColor;
			cachedGUIDrawBGCol  = cachedGUIPrevBGCol    * Instructions.BGColor;
			
			// prepare matrix if necessary
			if(Instructions.ApplyMatrix)
			{
				Instructions.PrepareMatrixAndRect(out cachedGUIDrawMatrix, out cachedDrawRect, cachedDrawRect);
				
				cachedGUIDrawMatrix = cachedGUIPrevMatrix   * cachedGUIDrawMatrix;
			}
			else
			{
				cachedGUIDrawMatrix = cachedGUIPrevMatrix;
			}
			
			if(Instructions.Group)
			{
				cachedGroupRect  = cachedDrawRect;
				cachedDrawRect.x = 0;
				cachedDrawRect.y = 0;
			}
			
		}
		
		void StartUpGUI()
		{
			if(Instructions.Group)
			{
				GUI.BeginGroup(cachedGroupRect);
			}
			
			ApplyGUIState();
		}
		
		void WrapUpGUI()
		{
			if(Instructions.Group)
			{
				GUI.EndGroup();
			}
			
			// reset the current gui state
			ResetGUIState();
		}	
		
		void ApplyGUIState()
		{
			GUI.color           = cachedGUIDrawCol;
			GUI.contentColor    = cachedGUIDrawFGCol;
			GUI.backgroundColor = cachedGUIDrawBGCol;
			GUI.matrix          = cachedGUIDrawMatrix;			
		}
		
		void ResetGUIState()
		{
			GUI.color           = cachedGUIPrevCol;
			GUI.contentColor    = cachedGUIPrevFGCol;
			GUI.backgroundColor = cachedGUIPrevBGCol;
			GUI.matrix          = cachedGUIPrevMatrix;			
		}
		
		void RegisterInput()
		{
			UIInputManager.AddToStack( this, InputRect );
		}
		#endregion
		
		#region Refreshing
		
		void RefreshGUIElements()
		{
			Cache_Parent();
			Cache_Renderers();
			Cache_InputReceivers();
			Cache_Children();
		}
		
		void Cache_Renderers()
		{
			cachedRenderers = GetAttachedInterfaces<IRenderable>(this) ;
		}
		
		void Cache_Children()
		{
			List<ComponentDrawer> tmpChildren = new List<ComponentDrawer>();
			
			Transform tmpT          = transform;
			int       tmpChildCount = tmpT.childCount;
			
			for (int i = 0; i < tmpChildCount; i++) {
				ComponentDrawer tmpD = tmpT.GetChild(i).GetComponent<ComponentDrawer>();
				
				if(tmpD != null)
				{
					tmpChildren.Add(tmpD);
				}
			}
			
			// sort data here
			tmpChildren.Sort(CompareDrawerByDepth);
			
			cachedChildren = tmpChildren.ToArray();
		}
		
		void Cache_InputReceivers()
		{
			cachedInputReceivers = GetAttachedInterfaces<ITouchReceiver>(this);
			
			//Component[] tmpInputReceivers = GetComponents(typeof(ITouchReceiver));
			//
			//cachedInputReceivers = new ITouchReceiver[tmpInputReceivers.Length];
			//
			//for (int i = 0; i < cachedInputReceivers.Length; i++) {
			//	cachedInputReceivers[i] = (ITouchReceiver)tmpInputReceivers[i];
			//}
		}		
		
		void Cache_Parent()
		{
			Transform tmpParent = this.transform.parent;
			
			if(cachedParent != null && tmpParent != cachedParent.transform)
			{
				cachedParent = null;
			}
			
			if(cachedParent == null && tmpParent != null)
			{
				cachedParent = tmpParent.GetComponent<ComponentDrawer>();
			}			
		}
		
		#endregion
		
		#endregion
		
		#region InterfaceImplementations
		
		#region ITouchReceiver
		
		void ITouchReceiver.OnTouchDown( Vector2 atScreenPos)
		{
			for (int i = 0; i < cachedInputReceivers.Length; i++) {
				if(cachedInputReceivers[i] != null)
				{
					cachedInputReceivers[i].OnTouchDown(atScreenPos);
				}
			}
		}
		void ITouchReceiver.OnTouch( Vector2 atScreenPos, bool onReceiver, Vector2 wDelta)
		{
			for (int i = 0; i < cachedInputReceivers.Length; i++) {
				if(cachedInputReceivers[i] != null)
				{
					cachedInputReceivers[i].OnTouch(atScreenPos, onReceiver, wDelta);
				}
			}
		}
		void ITouchReceiver.OnTouchUp( Vector2 atScreenPos, bool onReceiver)
		{
			for (int i = 0; i < cachedInputReceivers.Length; i++) {
				if(cachedInputReceivers[i] != null)
				{
					cachedInputReceivers[i].OnTouchUp(atScreenPos, onReceiver);
				}
			}
		}
		
		#endregion
		
		#endregion
		
		#region Misc
		
		private static T[] GetAttachedInterfaces<T>(Component fromComponent)                        where T : class { return GetAttachedInterfaces<T>(fromComponent, false); }
		private static T[] GetAttachedInterfaces<T>(Component fromComponent, bool includeComponent) where T : class
		{
			if(fromComponent == null) { return new T[0]; }
			
			Component[] tmpFoundComponents = fromComponent.GetComponents(typeof(T));
			
			int tmpFromComponentIndex   = -1;
			int tmpRetLength            = tmpFoundComponents.Length;
			
			if(!includeComponent)
			{
				// see if we have the component
				for (int i = 0; i < tmpFoundComponents.Length; i++) {
					if(tmpFoundComponents[i] == fromComponent)
					{
						tmpRetLength--;
						tmpFromComponentIndex = i;
						break;
					}
				}
			}
			
			T[] ret = new T[tmpRetLength];
			
			int j = 0;
			for (int i = 0; i < tmpFoundComponents.Length; i++) {
				
				if(tmpFromComponentIndex == i) { continue; }
				
				T tmp       = (T)(object)tmpFoundComponents[i];
				ret[j++]    = tmp;
			}
			
			return ret;
		}
		
		// depth comparison
		private static int CompareDrawerByDepthDesc(ComponentDrawer x, ComponentDrawer y) { return -CompareDrawerByDepth(x,y); }
		private static int CompareDrawerByDepth(ComponentDrawer x, ComponentDrawer y)
		{
			if(x == null)
			{
				if      (y == null)             { return 0;  }
				else                            { return -1; } // y is greater
			}
			else
			{
				if      (y == null)             { return 1;  } // x is greater
				else if (x.Instructions.Depth > y.Instructions.Depth)     { return 1;  } // x is greater
				else if (x.Instructions.Depth < y.Instructions.Depth)     { return -1; } // y is greater
				else                            { return 0;  }
			}
		}
		
		#endregion
		
	}
	
}


