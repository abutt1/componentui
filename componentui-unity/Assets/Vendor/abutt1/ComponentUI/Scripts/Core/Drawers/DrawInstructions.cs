using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using abutt1.DIGUI;

namespace abutt1.ComponentUI
{
	[System.Serializable]
	public class DrawInstructions
	{
		public  Alignment                   Alignment               = new Alignment();
		
		public  int                         Depth                   = 0;
		
		// color instructions
		public  Color                       Color                   = new Color(1,1,1,1);
		public  Color                       FGColor                 = new Color(1,1,1,1);
		public  Color                       BGColor                 = new Color(1,1,1,1);
		
		public  bool                        Group                   = false;
		
		// matrix instructions
		/// <summary>
		/// Whether or not to apply matrix transformations. Note: if matrix transformations are applied, input will be invalid.
		/// </summary>
		public  bool                        ApplyMatrix             = false;
		public  float                       Rotation                = 0;
		public  Vector2                     Scale                   = new Vector2(1,1);
		public  Vector2                     Pivot                   = new Vector2(0.5f,0.5f);
		
		public void PrepareMatrixAndRect(out Matrix4x4 mat, out Rect rect, Rect initialRect)
		{
			// Unity GUI does not like scales of 0, so this is a quick hack to still hopefully hide the content
			Vector2 withScale = Scale;
			if(withScale.x == 0 || withScale.y == 0)	{	withScale.x = 0.0001f; withScale.y = 0.0001f;}
			
			float pivotX    = initialRect.width     * Pivot.x;
			float pivotY    = initialRect.height    * Pivot.y;
			
			Vector3 s       = new Vector3(withScale.x,              withScale.y,                1f);
			Vector3 t       = new Vector3(initialRect.x + pivotX,   initialRect.y + pivotY,     0f);
			Quaternion r    = Quaternion.Euler(0,0,Rotation);
			
			initialRect.x   = -pivotX;
			initialRect.y   = -pivotY;
			
			mat             = Matrix4x4.TRS(t, r, s);
			rect            = initialRect;
		}
		
	}
}
