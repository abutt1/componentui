using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using abutt1.ProcessedInput;
using System.Linq;

namespace abutt1.ComponentUI
{
	
	public class UIInputManager
	{
		private static ButtonPerformerBehaviour behaviour;
		private static UIInputManager instance;
		
		private static UIInputManager Instance
		{
			get
			{
				if(instance == null)
				{
					instance = new UIInputManager();
				}
				
				if(behaviour == null)
				{
					instance.InitPerformerBehaviour();
				}
				
				return instance;
			}
		}

		private ButtonStack             stack                   = new ButtonStack();
		
		private ButtonInfo              activeButtonInfo;   // currently active button (if one..)
		private ButtonInfo              prevButtonInfo;     // previously active control (if one..)

		#region Interface
		
		public  static bool             HasFocusingControl              { get { return (CurrentlyFocusingControl  != null);   } }
		public  static bool             HasLastFocusedControl           { get { return (LastFocusedControl != null);     } }
		
		public  static ITouchReceiver   CurrentlyFocusingControl        { get { return Instance.activeButtonInfo.Button; } }
		public  static ITouchReceiver   LastFocusedControl              { get { return Instance.prevButtonInfo.Button;   } }
		
		
		public  static void AddToStack(ITouchReceiver button, Rect rect)
		{
			// maybe parse out null refs...
			//if(button.Button == null) { return; }
			// maybe parse out offscreen rects...
			
			AddToStack( new ButtonInfo(button, rect) );
		}
		public  static void AddToStack(ButtonInfo button)
		{
			Instance.stack.AddToStack(button);
		}
		
		public  static void UnfocusLastControl()
		{
			Instance.prevButtonInfo.Button = null;
		}
		
		#endregion
		
		#region Logic		
		void CheckButtonsLogic(ITouch[] wTouches)
		{
			// see if we have a touch, if we don't why bother...
			if(wTouches == null )       { return; }
			if(wTouches.Length == 0)    { return; }
			
			ITouch currentTouch = wTouches[0];

			Vector2 curMousePos = currentTouch.CurrentPos;
						
			if(activeButtonInfo.Button != null)
			{
				// if we have an active button already
				bool mouseOverButton = activeButtonInfo.Rect.Contains( curMousePos );
				
				if(currentTouch.CurrentState == TouchState.Active)
				{
					// see if we are hovering/not
					activeButtonInfo.Button.OnTouch(curMousePos, mouseOverButton, currentTouch.CurrentDelta);
				}
				else
				{
					// see if we have released on the button..
					// handle the button
					activeButtonInfo.Button.OnTouchUp( curMousePos, mouseOverButton );					
					
					if(mouseOverButton)
					{
						prevButtonInfo = activeButtonInfo;
					}
					else
					{
						// the button has failed...
						prevButtonInfo.Button = null;
					}
					
					activeButtonInfo.Button = null;
				}
			}
			else
			{
				// go through the stack and find a new active button if mouse down
				if ( currentTouch.CurrentState == TouchState.Down )
				{
					ButtonInfo curButtonInfo;
					while( stack.Pop( out curButtonInfo ) )
					{
						if(curButtonInfo.Button == null) { continue; }
						if(curButtonInfo.Rect.Contains( curMousePos ))
						{
							activeButtonInfo = curButtonInfo;
							
							// Button Down logic...
							activeButtonInfo.Button.OnTouchDown( curMousePos );
							
							break;
						}
					}
					
				}
			}
						
		}

		void InitPerformerBehaviour()
		{
			behaviour = GameObject.FindObjectOfType(typeof(ButtonPerformerBehaviour)) as ButtonPerformerBehaviour;
			
			if(behaviour == null)
			{
				GameObject tmpGO = new GameObject("abutt1.ComponentUI.ButtonPerformerBehaviour");
				behaviour = tmpGO.AddComponent<ButtonPerformerBehaviour>();
			}
			
			behaviour.BP = this;
		}
		
		#endregion
		
		public void CheckButtons(ITouch[] wTouches)
		{
			CheckButtonsLogic(wTouches);
		}
		
		public void ClearButtons()
		{
			stack.ClearStack();
		}
		
	}
	
	public class ButtonPerformerBehaviour : MonoBehaviour
	{
		public  UIInputManager  BP;
		
		void OnEnable()
		{
			StartCoroutine(PerformCheckAtEndOfFrame());
		}
		
		void Update()
		{
			if(BP == null) { Destroy(this.gameObject); return; }
			BP.ClearButtons();
		}
		
		IEnumerator PerformCheckAtEndOfFrame()
		{
			while(true)
			{
				yield return new WaitForEndOfFrame();
				if(BP != null)
				{
					BP.CheckButtons(InputController.SharedStatus.ActiveTouches.ToArray());
				}
			}
		}
	}
	
}
