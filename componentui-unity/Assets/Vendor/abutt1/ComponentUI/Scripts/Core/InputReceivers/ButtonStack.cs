using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace abutt1.ComponentUI
{
	public class ButtonStack {
		
		private Stack<ButtonInfo> stack = new Stack<ButtonInfo>();

		public  void AddToStack(ButtonInfo button)
		{
			stack.Push(button);
		}
		public  void ClearStack()
		{
			stack.Clear();
		}		
		public  bool Pop(out ButtonInfo nextButton)
		{
			if(stack.Count > 0)
			{
				nextButton = stack.Pop();
				return true;
			}
			else
			{
				nextButton = new ButtonInfo();
				return false;
			}
		}
	}
	
	public struct ButtonInfo
	{
		public  ButtonInfo(ITouchReceiver withButton, Rect withRect)
		{
			Button  = withButton;
			Rect    = withRect;
		}
		
		public  ITouchReceiver      Button;
		public  Rect                Rect;
	}
		
}
