using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace abutt1.ComponentUI
{
	[System.Serializable]
	public class ButtonTarget {
		public  GameObject  TargetGO;
		public  string      TargetFunctionName;
		
		public  bool SendMessageIfAble()
		{
			if(TargetGO == null)                            { return false; }
			if(string.IsNullOrEmpty(TargetFunctionName))    { return false; }
			
			TargetGO.SendMessage(TargetFunctionName, SendMessageOptions.DontRequireReceiver);
			return true;
		}
	}
	
	
	
}
