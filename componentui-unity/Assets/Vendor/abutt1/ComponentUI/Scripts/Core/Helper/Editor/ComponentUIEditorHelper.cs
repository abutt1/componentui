using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Reflection;
using System.Collections.Generic;

namespace abutt1.ComponentUI
{
	
	public class ComponentUIEditorHelper
	{
		public static string[] GetPublicParameterlessFunctionsForGO(GameObject go, string[] customPreStrings = null)
		{
			List<string> ret = new List<string>();
			
			if(customPreStrings != null)
			{
				for (int i = 0; i < customPreStrings.Length; i++) {
					ret.Add(customPreStrings[i]);
				}
			}
			
			if(go == null) { return ret.ToArray(); }
	
			// find all public parameterless functions
			Component[] components = go.GetComponents<Component>();
			
			for (int i = 0; i < components.Length; i++) {
				MethodInfo[] tmpMethods = components[i].GetType().GetMethods(BindingFlags.Public | BindingFlags.Instance);
				for (int j = 0; j < tmpMethods.Length; j++) {
					
					MethodInfo curMethod = tmpMethods[j];
					
					if(ret.Contains(curMethod.Name))                                        { continue; } // already have this method
					if(curMethod.GetParameters().Length > 0)                                { continue; } // only want parameterless funcs
					if(curMethod.MemberType == MemberTypes.Constructor)                     { continue; } // no constructors thanks
					if(curMethod.MemberType == MemberTypes.Property)                        { continue; } // no properties would be nice, (i think this will not work..)
					if(curMethod.IsSpecialName == true)                                     { continue; } // don't want special name funcs
					if(curMethod.DeclaringType.IsAssignableFrom(typeof(MonoBehaviour)))     { continue; } // no monobehaviour declared funcs
					if(curMethod.DeclaringType == (typeof(Transform)))                      { continue; } // no monobehaviour declared funcs
					if(StringIsAMonoMessageFunctionName(curMethod.Name))                    { continue; } // don't want mono messages
					
					
					ret.Add(curMethod.Name);
				}
			}
			return ret.ToArray();
		}
		
		public static string[] GetStyleNamesFromSkin(GUISkin skin, string[] customPreStrings = null)
		{
			List<string> ret = new List<string>();
			
			if(customPreStrings != null)
			{
				for (int i = 0; i < customPreStrings.Length; i++) {
					ret.Add(customPreStrings[i]);
				}
			}
			
			if(skin == null)    { return ret.ToArray(); }
			
			IEnumerator skinEnumerator = skin.GetEnumerator();
			
			if(skinEnumerator == null)  { return ret.ToArray(); }
			
			while(skinEnumerator.MoveNext())
			{
				GUIStyle tmpStyle = skinEnumerator.Current as GUIStyle;
				
				if(tmpStyle != null)
				{
					ret.Add(tmpStyle.name);
				}
			}
			
			return ret.ToArray();
		}
		
		
		public static int GetIndexOfString(string str, string[] inArray)
		{
			if(inArray != null)
			{
				for (int i = 0; i < inArray.Length; i++) {
					if(str == inArray[i])
					{
						return i;
					}
				}
			}
			return -1;
		}
		
		public static bool StringIsAMonoMessageFunctionName(string funcName)
		{
			if(GetIndexOfString(funcName, MonoBehaviourExclusionFunctions) != -1)
			{
				return true;
			}
			return false;
		}
		
		public static string[] MonoBehaviourExclusionFunctions
		{
			get
			{
				// correct as of unity 4.2.0
				return new string[]
				{
					"Awake",
					"FixedUpdate",
					"LateUpdate",
					"OnAnimatorIK",
					"OnAnimatorMove",
					"OnApplicationFocus",
					"OnApplicationPause",
					"OnApplicationQuit",
					"OnAudioFilterRead",
					"OnBecameInvisible",
					"OnBecameVisible",
					"OnCollisionEnter",
					"OnCollisionExit",
					"OnCollisionStay",
					"OnConnectedToServer",
					"OnControllerColliderHit",
					"OnDestroy",
					"OnDisable",
					"OnDisconnectedFromServer",
					"OnDrawGizmos",
					"OnDrawGizmosSelected",
					"OnEnable",
					"OnFailedToConnect",
					"OnFailedToConnectToMasterServer",
					"OnGUI",
					"OnJointBreak",
					"OnLevelWasLoaded",
					"OnMasterServerEvent",
					"OnMouseDown",
					"OnMouseDrag",
					"OnMouseEnter",
					"OnMouseExit",
					"OnMouseOver",
					"OnMouseUp",
					"OnMouseUpAsButton",
					"OnNetworkInstantiate",
					"OnParticleCollision",
					"OnPlayerConnected",
					"OnPlayerDisconnected",
					"OnPostRender",
					"OnPreCull",
					"OnPreRender",
					"OnRenderImage",
					"OnRenderObject",
					"OnSerializeNetworkView",
					"OnServerInitialized",
					"OnTriggerEnter",
					"OnTriggerExit",
					"OnTriggerStay",
					"OnValidate",
					"OnWillRenderObject",
					"Reset",
					"Start",
					"Update",
				};
			}
		}
	}
	
}
