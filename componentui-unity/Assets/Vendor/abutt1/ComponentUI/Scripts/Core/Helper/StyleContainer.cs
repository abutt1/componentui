using UnityEngine;
using System.Collections;

namespace abutt1.ComponentUI
{
	[System.Serializable]
	/// <summary>
	/// A container that allows you to grab a style from a skin.
	/// </summary>
	public class SkinStyleContainer {

		public  GUISkin     Skin;
		public  string      SkinStyleName;
		
		/// <summary>
		/// Gets the style.
		/// </summary>
		public  GUIStyle    Style
		{
			get
			{
				GUIStyle ret = null;
				if(Skin != null)
				{
					ret = Skin.FindStyle(SkinStyleName);
				}
				return ret;
			}
		}
		
		/// <summary>
		/// Gets the style. If no style is found, will return GUIStyle.none
		/// </summary>
		public  GUIStyle    StyleSafe
		{
			get
			{
				GUIStyle ret = Style;
				if(ret == null)
				{
					ret = GUIStyle.none;
				}
				return ret;
			}
		}
	}
	
	[System.Serializable]
	/// <summary>
	/// A container that allows you to grab a style from either a SkinStyleContainer or a backup GUIStyle
	/// </summary>
	public class CustomSkinStyleContainer {

		public  SkinStyleContainer  SkinContainer;
		public  GUIStyle            CustomStyle;
		
		/// <summary>
		/// Gets the style.
		/// </summary>		
		public  GUIStyle Style
		{
			get
			{
				GUIStyle ret = null;
				if(SkinContainer != null)
				{
					ret = SkinContainer.Style;
				}
				
				if(ret == null)
				{
					ret = CustomStyle;
				}
				
				return ret;
			}
		}
		
		/// <summary>
		/// Gets the style. If no style is found, will return GUIStyle.none
		/// </summary>
		public  GUIStyle    StyleSafe
		{
			get
			{
				GUIStyle ret = Style;
				if(ret == null)
				{
					ret = GUIStyle.none;
				}
				return ret;
			}
		}
		
	}
	
}
