using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Reflection;
using System.Collections.Generic;

namespace abutt1.ComponentUI
{
	
	[CustomPropertyDrawer (typeof(ButtonTarget))]
	public class ButtonTargetDrawer : PropertyDrawer
	{
		const int DropDownWidth = 75;
		
		public override void OnGUI (Rect position, SerializedProperty property, GUIContent label)
		{
			SerializedProperty TargetGO             = property.FindPropertyRelative("TargetGO");
			SerializedProperty TargetFunctionName   = property.FindPropertyRelative("TargetFunctionName");
			
			// draw the object GUI
			Rect drawRect   =  position;
			drawRect.width  -= DropDownWidth;
			
			// attach/detach GO
			EditorGUI.PropertyField(drawRect, TargetGO);
			
			// draw the functions gui
			drawRect.x      =  drawRect.xMax;
			drawRect.xMax   =  position.xMax;
			
			// Grab the current methods -> first element is always "- NONE -"
			string[] publicFuncs = ComponentUIEditorHelper.GetPublicParameterlessFunctionsForGO(TargetGO.objectReferenceValue as GameObject, new string[] {"- NONE -"});
			
			int currentFuncIndex = ComponentUIEditorHelper.GetIndexOfString(TargetFunctionName.stringValue, publicFuncs);
			
			if(publicFuncs != null && publicFuncs.Length > 0)
			{
				EditorGUI.BeginChangeCheck();
				currentFuncIndex = EditorGUI.Popup(drawRect, currentFuncIndex, publicFuncs );
				
				if(EditorGUI.EndChangeCheck())
				{
					// if it's the first element, it's actualy null
					string newFuncValue;
					if(currentFuncIndex == 0) {
						newFuncValue = null;
					} else {
						newFuncValue = publicFuncs[currentFuncIndex];
					}
					
					TargetFunctionName.stringValue = newFuncValue;
				}
			}
		}
		
		public override float GetPropertyHeight (SerializedProperty property, GUIContent label)
		{
			return base.GetPropertyHeight (property, label);
		}
		
	}
	
}
