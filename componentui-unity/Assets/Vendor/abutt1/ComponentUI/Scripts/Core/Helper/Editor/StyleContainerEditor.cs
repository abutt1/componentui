using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Reflection;
using System.Collections.Generic;

namespace abutt1.ComponentUI
{
	
	[CustomPropertyDrawer (typeof(SkinStyleContainer))]
	public class StyleContainerDrawer : PropertyDrawer
	{
		const int DropDownWidth = 75;
		
		public override void OnGUI (Rect position, SerializedProperty property, GUIContent label)
		{
			SerializedProperty TargetSkin               = property.FindPropertyRelative("Skin");
			SerializedProperty TargetSkinStyleName      = property.FindPropertyRelative("SkinStyleName");
			
			// draw the object GUI
			Rect drawRect   =  position;
			drawRect.width  -= DropDownWidth;
			
			
			EditorGUI.BeginChangeCheck();
			Object skinCheckObject = EditorGUI.ObjectField(drawRect, property.name, TargetSkin.objectReferenceValue, typeof(GUISkin), false);
			if( EditorGUI.EndChangeCheck() )
			{
				TargetSkin.objectReferenceValue = skinCheckObject as GUISkin;
			}
			
			GUISkin curSkin = TargetSkin.objectReferenceValue as GUISkin;
			
			// if we have a Skin, we draw a popup selector
			if(curSkin != null)
			{
				drawRect.x      =  drawRect.xMax;
				drawRect.xMax   =  position.xMax;				
				
				// do a popup for selected style
				string[] possibleStyles = ComponentUIEditorHelper.GetStyleNamesFromSkin(curSkin, new string[]{"- NONE -"} );
				
				if(possibleStyles != null && possibleStyles.Length > 0)
				{
					int curStyleIndex = ComponentUIEditorHelper.GetIndexOfString(TargetSkinStyleName.stringValue, possibleStyles);
					
					EditorGUI.BeginChangeCheck();
					curStyleIndex = EditorGUI.Popup(drawRect, curStyleIndex, possibleStyles);
					if(EditorGUI.EndChangeCheck())
					{
						// if it's the first element, it's actualy null
						string newStyleValue;
						if(curStyleIndex == 0) {
							newStyleValue = null;
						} else {
							newStyleValue = possibleStyles[curStyleIndex];
						}
						
						TargetSkinStyleName.stringValue = newStyleValue;
					}
				}
			}
			
		}
		
		public override float GetPropertyHeight (SerializedProperty property, GUIContent label)
		{
			return base.GetPropertyHeight (property, label);
		}
		
	}
	
}
