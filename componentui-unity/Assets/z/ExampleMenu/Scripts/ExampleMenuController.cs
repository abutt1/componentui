using UnityEngine;
using System.Collections;

public class ExampleMenuController : MonoBehaviour {
	
	public  ExampleMenuUIState  UIState_Title;
	public  ExampleMenuUIState  UIState_MainMenu;
	public  ExampleMenuUIState  UIState_Play;
	public  ExampleMenuUIState  UIState_Options;	
	
	public  ExampleMenuUIState  UIState_Initial;
	
	private ExampleMenuUIState  currentState;
	
	void Start()
	{
		SwitchToState(UIState_Initial);
	}
	
	#region Interface

	#region Requests
	
	public void SwitchToMainMenu()
	{
		Debug.Log("ExampleMenuController.SwitchToMainMenu");
		SwitchToState(UIState_MainMenu);
	}

	public void SwitchToPlay()
	{
		Debug.Log("ExampleMenuController.SwitchToPlay");
		SwitchToState(UIState_Play);
	}
	
	public void SwitchToOptions()
	{
		Debug.Log("ExampleMenuController.SwitchToOptions");
		SwitchToState(UIState_Options);
	}
	
	#endregion
	
	void SwitchToState(ExampleMenuUIState toState)
	{
		if(toState == null)                                     { return; } // don't transfer to a null state
		if(currentState == toState)                             { return; } // don't transfer to the same state
		if(currentState != null && currentState.Transitioning)  { return; } // don't transfer if currently transitioning
		
		toState.SetState(true);
		if(currentState != null) 
		{ 
			currentState.SetState(false); 
		}
		currentState = toState;
	}
	
	#endregion
	
}
