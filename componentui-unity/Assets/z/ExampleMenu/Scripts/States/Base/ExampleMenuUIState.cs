using UnityEngine;
using System.Collections;


public interface IExampleMenuUIState {
	
	bool  Transitioning { get; }
	void  SetState(bool toState);
}

public abstract class ExampleMenuUIState : MonoBehaviour, IExampleMenuUIState {
	
	protected bool  destinationState;
	protected bool  transitioning;
	
	public bool Transitioning 
	{
		get 
		{
			return transitioning;
		}
	}
	
	public virtual void SetState (bool toState)
	{
		destinationState = toState;
	}
}
