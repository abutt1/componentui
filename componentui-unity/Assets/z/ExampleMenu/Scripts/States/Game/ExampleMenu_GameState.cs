using UnityEngine;
using System.Collections;

public class ExampleMenu_GameState : ExampleMenuUIState {
	
	public  ExampleMenuController   MenuController;
	
	public  Animation               Anim;
	public  string                  AnimInName;
	public  string                  AnimOutName;
	
	public  float                   AnimInSpeed     = 1;
	public  float                   AnimOutSpeed    = 1;
	
	public override void SetState (bool toState)
	{
		if(destinationState == toState) { return; }
		
		base.SetState(toState);
		
		if(Anim != null)
		{
			if(destinationState)
			{
				// ensure its on
				Anim.gameObject.SetActive(true);
				
				// play in anim
				Anim[AnimInName].normalizedSpeed = AnimInSpeed;
				Anim.Play(AnimInName, PlayMode.StopAll);
			}
			else
			{
				// play out anim
				Anim[AnimOutName].normalizedSpeed = AnimOutSpeed;
				Anim.Play(AnimOutName, PlayMode.StopAll);
				
				// turn off the game
			}
			transitioning = true;
		}
	}
	
	void Update()
	{
		if(Anim != null)
		{
			if(!Anim.isPlaying)
			{
				transitioning = false;
				
				if(destinationState == false)
				{
					Anim.gameObject.SetActive(false);
				}
			}
		}
		
		// for debugging
		if(destinationState && Time.frameCount % 100 == 0)
		{
			OnStartButtonPressed();
		}
	}
	
	#region ButtonPresses
	
	public void OnStartButtonPressed()
	{
		// start up the game
	}

	public void OnExitButtonPressed()
	{
		if(MenuController != null)
		{
			MenuController.SwitchToMainMenu();
		}
	}
	
	#endregion
}
